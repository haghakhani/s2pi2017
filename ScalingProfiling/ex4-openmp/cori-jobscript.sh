#!/bin/bash -l
#SBATCH -N 1
#SBATCH -t 10
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex4
##SBATCH --reservation=STPI_hsw

# In this exercise, we will run our TAU-instrumented OpenMP version of miniFE
# with 1 MPI rank but a few OpenMP thread counts, and use TAU to identify issues
# in the OpenMP scaling

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-Training/s2pi2017/ScalingProfiling}

module load tau

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex4-openmp/build/miniFE-omp-tau.x

# A 200x200x200 miniFE job should finish within 5 minutes on a single core 
# of a Cori Haswell node:
sz=200
cmd="$ex -nx $sz -ny $sz -nz $sz"

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each OpenMP thread to have its own core. The following recipe calculates how
# many OpenMP threads we can run:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cores_per_node=$((SLURM_CPUS_ON_NODE/hyperthreads_per_core))
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_omp_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))

# for OpenMP scaling, we'll only use 1 MPI rank:
nranks=1

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

# .. and prepare a gnuplot data file:
timing_file=$SLURM_SUBMIT_DIR/timings-$SLURM_JOB_ID.dat
echo "#nthreads" $'\t' "bm_time" $'\t' "walltime" >> $timing_file

# let's look at the profile with 1, 2, and 16 threads
for OMP_NUM_THREADS in 1 2 16 ; do
  export OMP_NUM_THREADS

  # CPU affinity is important with OpenMP:
  export OMP_PROC_BIND=true
  export OMP_PLACES=cores

  label=tau_profile-sz${sz}-${SLURM_NNODES}n-${OMP_NUM_THREADS}omp-$SLURM_JOB_ID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex4-openmp/$label
  mkdir -p $rundir
  cd $rundir

  tm=$( { time srun -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr ; } 2>&1 )

  # if the run succeeded, we should have a .yaml file in the $rundir with various
  # metrics about how the run went. We'll extract its report of the total run
  # time, and append a gnuplot data file with the timing info:
  bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
  echo "  $OMP_NUM_THREADS" $'\t' $bm_tm $'\t' $tm >> $timing_file

done
echo "finished miniFE OpenMP runs at `date`"

