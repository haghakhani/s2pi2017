program omp_hello_world

    use omp_lib

    implicit none

    integer :: thread_id

    !$omp parallel private(thread_id)

        ! Getting the individual thread number in 'id'
        ! using a runtime routine
        thread_id = omp_get_thread_num()

        ! Print Hello world from each thread
        write ( *, '(a,i4)' ) 'Hello World from thread ', thread_id

        ! Making sure each thread has printed "Hello World",
        ! before we move forward
        !$omp barrier

        ! Making the master thread to identify itself!
        !$omp master
            write ( *, '(a,i4)' ) 'Hi, I am MASTER, my id is always ', thread_id
        !$omp end master

        ! Now print Hello OpenMP from each thread
        ! (after all everybody needs to learn OpenMP!)
        write ( *, '(a,i4)' ) 'Hello OpenMP from thread  ', thread_id

    !$omp end parallel

    stop

  end program omp_hello_world

